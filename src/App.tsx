import React, { useState, useEffect } from "react";
import Header from "./Header";
import Footer from "./Footer";
import "../public/index.css";
import { DateTime } from "luxon";

interface Cat {
  name: String;
  age: number;
}

function App() {
  let murko: Cat = {
    name: "Murko",
    age: 5,
  };
  let ryzhyk = {
    name: "Ryzhyk",
    age: 6,
  };
  const cats: Array<Cat> = [murko, ryzhyk];
  const [count, setCount] = useState(0);
  const now: DateTime = DateTime.now();
  const newYear: DateTime = DateTime.fromObject({ month: 12, day: 31 });

  useEffect(() => {
    const timer: NodeJS.Timeout = setTimeout(() => setCount(count + 1), 1000);
    return () => clearTimeout(timer);
  }, [count, setCount]);

  return (
    <div className='App'>
      <Header />
      <div className='root'>
        <div>
          Page has been open for <code>{count}</code> seconds.
        </div>
        <div>Cat name is: {murko.name}</div>
        <div>Yesterday was - {now.minus({ days: 1 }).toLocaleString()}</div>
        <div>New Year is - {newYear.toLocaleString()}</div>
        <div>
          <p>Cats:</p>
          {cats.map((cat) => (
            <div>{cat.name}</div>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
