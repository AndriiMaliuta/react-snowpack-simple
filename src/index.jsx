import React from "react";
import ReactDOM from "react-dom";
import App from "./App.jsx";

/* Add JavaScript code here! */
console.log("Hello World! You did it! Welcome to Snowpack :D");

ReactDOM.render(<App />, document.getElementById("root"));
